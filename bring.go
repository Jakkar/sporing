package main

import (
	"encoding/json"
	"errors"
	"net/http"
	"regexp"
	"strings"
)

// https://developer.bring.com/api/tracking/
const trackingURL = "https://tracking.bring.com/api/v2/tracking.json?q="

// TODO: make this a flag instead
const testValue = "TESTPACKAGE-AT-PICKUPPOINT"

type apiResponse struct {
	ConsignmentSet []struct {
		ConsignmentId string `json:"consignmentId"`
		PackageSet    []struct {
			HeightInCm  int     `json:"heightInCm"`
			LengthInCm  int     `json:"lengthInCm"`
			WidthInCm   int     `json:"widthInCm"`
			WeightInKgs float32 `json:"weightInKgs"`
			ProductName string  `json:"productName"`
			SenderName  string  `json:"senderName"`

			RecipientAddress struct {
				Country    string `json:"country"`
				City       string `json:"city"`
				PostalCode string `json:"postalCode"`
			} `json:"recipientAddress"`

			RecipientHandlingAddress struct {
				Country    string `json:"country"`
				City       string `json:"city"`
				PostalCode string `json:"postalCode"`
			} `json:"recipientHandlingAddress"`

			EventSet []struct {
				Country     string `json:"country"`
				City        string `json:"city"`
				PostalCode  string `json:"postalCode"`
				DisplayDate string `json:"displayDate"`
				DisplayTime string `json:"displayTime"`
				Description string `json:"description"`
			} `json:"eventSet"`
		} `json:"packageSet"`

		Error struct {
			Code    int    `json:"code"`
			Message string `json:"message"`
		} `json:"error"`
	} `json:"consignmentSet"`
}

func getTrackingDetails(trackingNum string) (apiResponse, error) {

	if len(trackingNum) == 0 {
		return apiResponse{}, errors.New("no tracking number provided")
	}

	// check for tracking numbers like CS111111111NO (2 letters + 9 numbers + 2 letters)
	ptrnMixed, _ := regexp.Compile(`^[a-zA-Z]{2}[0-9]{9}[a-zA-Z]{2}$`)

	// check for tracking numbers like 370733344455566677 (starts with 3, 18 numbers long)
	ptrn3, _ := regexp.Compile(`^[3][0-9]{17}$`)

	// check for tracking numbers like 77073334445556667 (starts with 7, 17 numbers long)
	ptrn7, _ := regexp.Compile(`^[7][0-9]{16}$`)

	if !ptrnMixed.MatchString(trackingNum) &&
		!ptrn3.MatchString(trackingNum) &&
		!ptrn7.MatchString(trackingNum) &&
		trackingNum != testValue {
		return apiResponse{}, errors.New("invalid tracking number provided")
	}

	res, err := http.Get(trackingURL + trackingNum)
	if err != nil {
		return apiResponse{}, errors.New("web request failed. Are you connected to the internet? :)")

	}

	defer res.Body.Close()

	jsonRes := apiResponse{}
	json.NewDecoder(res.Body).Decode(&jsonRes)

	// because stupid api doesn't use status codes properly
	if jsonRes.ConsignmentSet[0].Error.Code != 0 {
		return apiResponse{}, errors.New(strings.ToLower(jsonRes.ConsignmentSet[0].Error.Message))
	}

	switch res.StatusCode {
	case 200:
		return jsonRes, nil
	default:
		return apiResponse{}, errors.New("recieved unknown response from tracking service")
	}
}
