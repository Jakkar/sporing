package main

import (
	"errors"
	"flag"
	"os"
	"strings"

	"github.com/pterm/pterm"
)

func main() {
	// --help
	flag.Usage = func() {
		pterm.Info.Printfln("Usage: %s <tracking number>", os.Args[0])
	}

	flag.Parse()

	if len(os.Args) < 2 {
		pterm.Error.Println(errors.New("no tracking number given"))
		return
	}

	trackingNumber := flag.Arg(0)

	waitingSpinner, _ := pterm.DefaultSpinner.WithShowTimer(false).WithRemoveWhenDone(true).Start("Getting tracking information...")
	results, err := getTrackingDetails(trackingNumber)
	if err != nil {
		pterm.Error.Println(err)
		return
	}
	waitingSpinner.Stop()

	// TODO: cases where several packages are sent in same consignment

	// print header
	pterm.DefaultHeader.
		WithBackgroundStyle(pterm.NewStyle(pterm.BgDefault)).
		WithTextStyle(pterm.NewStyle(pterm.FgDefault)).
		Println("Tracking Details for", results.ConsignmentSet[0].ConsignmentId)

	// print panels
	pterm.DefaultPanel.
		WithPanels(pterm.Panels{
			{
				{Data: createEventHistoryPanel(results)},
				{Data: createInfoPanel(results)},
			},
		}).
		Render()
}

func createEventHistoryPanel(packageInfo apiResponse) string {
	eventHistory := packageInfo.ConsignmentSet[0].PackageSet[0].EventSet

	// var eventHistoryContent strings.Builder
	uh := pterm.Panels{}

	for _, event := range eventHistory {
		uh = append(uh, []pterm.Panel{{Data: pterm.DefaultBox.
			WithTitle(event.DisplayDate+" "+event.DisplayTime).
			WithBoxStyle(pterm.NewStyle(pterm.BgDefault, pterm.FgDefault)).
			Sprint("Location: ",
				event.PostalCode, " ", event.City,
				"\n",
				"Description:\n",
				pterm.DefaultParagraph.
					WithMaxWidth(40).
					Sprint(event.Description),
			),
		}})
	}

	panels, _ := pterm.DefaultPanel.WithPanels(uh).Srender()

	return pterm.DefaultBox.
		WithTitle("Event History").
		WithBoxStyle(pterm.NewStyle(pterm.BgDefault, pterm.FgDefault)).
		Sprint(panels)
}

func createInfoPanel(packageInfo apiResponse) string {
	// package is reserved word >:(
	parcel := packageInfo.ConsignmentSet[0].PackageSet[0]

	var consignmentInformation strings.Builder

	consignmentInformation.WriteString(
		pterm.Sprintln("Sender:", parcel.SenderName),
	)
	consignmentInformation.WriteString(
		pterm.Sprintln("Recipient address:",
			parcel.RecipientAddress.PostalCode, " ", parcel.RecipientAddress.City,
		),
	)
	consignmentInformation.WriteString(
		pterm.Sprintln("Delivery address:",
			parcel.RecipientHandlingAddress.PostalCode, " ", parcel.RecipientHandlingAddress.City,
		),
	)
	consignmentInformation.WriteString(
		pterm.Sprintln("Product Name:", parcel.ProductName),
	)
	consignmentInformation.WriteString(
		pterm.Sprintln("Total Weight:", parcel.WeightInKgs, " kg"),
	)
	consignmentInformation.WriteString(
		pterm.Sprintln("Size:",
			parcel.WidthInCm, " x ", parcel.LengthInCm, " x ", parcel.HeightInCm, " cm",
		),
	)

	return pterm.DefaultBox.
		WithTitle("Consignment Information").
		WithBoxStyle(pterm.NewStyle(pterm.BgDefault, pterm.FgDefault)).
		Sprint(consignmentInformation.String())
}
